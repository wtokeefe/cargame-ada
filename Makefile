project_file=cargame.gpr
sources=$(project_file) $(wildcard src/*.ads) $(wildcard src/*.adb)
objects=$(wildcard obj/*.ali) $(wildcard obj/*.o) # NOTE: Don't touch the .dll and .a files in there!
gpr_options=$(project_file) -gnatm5 -gnatW8
gpr_debug_options=-gnata -XAuto_Exceptions=enabled
gpr_release_options=-O2 -gnatN
gnatprove_options=-XAuto_Exceptions=enabled
uname=$(uname)

ifeq ($(OS),Windows_NT)
	gpr_options += -XWindowing_System=windows
	gnatprove_options += -XWindowing_System=windows
else
	gpr_options += -XWindowing_System=x11
	gnatprove_options += -XWindowing_System=x11
endif

all: $(sources)
	mkdir -p bin obj
	gprbuild $(gpr_options) $(gpr_debug_options)

release: $(sources)
	gprbuild $(gpr_options) $(gpr_release_options)

clean: $(objects)
	rm -fv $(objects)

prove: $(sources)
	gnatprove -P$(project_file) $(gnatprove_options)
