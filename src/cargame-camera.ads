package Camera is
        View_Mtx : Matrix4;
        Position : Vector3;
        Target   : Vector3;
        Rotation : Vector2;

        procedure Reset;
        procedure Update;
end Camera;
