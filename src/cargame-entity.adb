with Ada.Containers;
with Ada.Text_IO;

with GL;
with GL.Uniforms;
with GL.Objects.Textures;
with GL.Objects.Textures.Targets;
with GL.Objects.Vertex_Arrays;
with GL.Objects.Buffers;

with Cargame.Globals;
with Cargame.Uniforms;
with Cargame.Util;
with Cargame.Vectors;

package body Cargame.Entity is

   use GL;

   ----------------------
   -- Send_Render_Data --
   ----------------------

   procedure Send_Render_Data(E : in Entity) is
      use GL.Objects.Textures, GL.Objects.Textures.Targets;
      use Ada.Containers;
      use Ada.Text_IO;

      Camera_Transform : constant Matrix4 := Uniforms.Camera_Transform.Get;
      Object_Transform : Matrix4 := Identity4;
      CamObj_Transform : Matrix4 := Identity4;
      Normal_Transform : Matrix3 := Identity3;
   begin
      Scale     (Object_Transform, E.Scale);
      Rotate    (Object_Transform, E.Rotation);
      Translate (Object_Transform, Vector3(E.Position));

      CamObj_Transform := Camera_Transform * Object_Transform;

      Uniforms.Object_Transform.Set(Object_Transform);
      Uniforms.CamObj_Transform.Set(CamObj_Transform);
      Uniforms.Normal_Transform.Set(Transpose(Inverse(Mat4_To_Mat3(CamObj_Transform))));
   end Send_Render_Data;

   ------------
   -- Rotate --
   ------------

   procedure Rotate(E : in out Entity; Angle : in Radians) is
   begin
      E.Rotation := E.Rotation + Angle;
   end Rotate;

   procedure Rotate(E : in out Entity; Angle : in Degrees) is
   begin
      E.Rotate(To_Radians(Angle));
   end Rotate;

   ------------------
   -- Set_Material --
   ------------------

   procedure Set_Material(M : in Material) is
      use GL.Uniforms;
      use GL.Objects.Textures, GL.Objects.Textures.Targets;
      use Ada.Text_IO;
   begin
      --  Put_Line("Setting texture to Material[" & Get_Material_Name(M) & "]");

      Uniforms.Material_Shininess.Set(M.Shininess);
      Uniforms.Material_Ambient.Set(M.Ambient_Light);

      if M.Diffuse_Texture.Initialized then
         --  Put_Line("Binding Diffuse_Texture := " & UInt'Image(M.Specular_Texture.Raw_ID));
         Uniforms.Diffuse_Map.Set(Globals.Diffuse_Map_ID);
         Set_Active_Unit(Globals.Diffuse_Map_ID);
         Texture_2D.Bind(M.Diffuse_Texture);
      end if;

      if M.Specular_Texture.Initialized then
         --  Put_Line("Binding Specular_Texture := " & UInt'Image(M.Specular_Texture.Raw_ID));
         Uniforms.Specular_Map.Set(Globals.Specular_Map_ID);
         Set_Active_Unit(Globals.Specular_Map_ID);
         Texture_2D.Bind(M.Specular_Texture);
      end if;

      --  Put_Line("Done. Maybe?");
   end Set_Material;

   ------------
   -- Render --
   ------------

   procedure Render(E : in Entity) is
      use GL.Objects.Vertex_Arrays;
      use GL.Objects.Buffers;
      use Ada.Text_IO;
      use Cargame.Util;
   begin

      E.Send_Render_Data;

      Bind(E.RVAO.Vao);
      Bind(Array_Buffer, E.RVAO.Vertex_Buf);
      Bind(Element_Array_Buffer, E.RVAO.Index_Buf);

      for Mtl of E.RVAO.Materials loop
         Set_Material(Mtl);
         Draw_Elements(Mode           => Triangles,
                       Index_Type     => UInt_Type,
                       Element_Offset => Mtl.First_Index,
                       Count          => Num_Indices(Mtl));
      end loop;

      Unbind_VAO;
   end Render;

   ----------
   -- Move --
   ----------

   procedure Move(E : in out Entity; Distance : in Distance_Type) is
   begin
      E.Position := E.Position + Distance;
   end Move;

   --------------
   -- Place_At --
   --------------

   procedure Place_At(E : in out Entity; Position : in Position_Type) is
   begin
      E.Position := Position;
   end Place_At;

   ------------------------
   -- Entity_Vector_With --
   ------------------------

   function Entity_Vector_With(Initial_Elements : in Array_Of_Entity)
                              return Vector_Of_Entity is
   begin
      return Vec : Vector_Of_Entity do
        for E of Initial_Elements loop
           Vec.Append(E);
        end loop;
      end return;
   end Entity_Vector_With;

   ---------------------------
   -- Append_Default_Entity --
   ---------------------------

   function Append_Default_Entity(V : in out Vector_Of_Entity) return Natural is
      E : Entity;
   begin
      V.Append(E);
      return V.Last_Index;
   end Append_Default_Entity;

   procedure Send_Render_Data(L : in Light) is
      use Cargame.Uniforms;
      use GL.Uniforms;
   begin
      Uniforms.Light_Position.Set(Vector3(L.Position));
      Uniforms.Light_Ambient.Set(Vector3'(X => L.Ambient(R), Y => L.Ambient(G), Z => L.Ambient(B)));
      Uniforms.Light_Diffuse.Set(Vector3'(X => L.Diffuse(R), Y => L.Diffuse(G), Z => L.Diffuse(B)));
      --  Uniforms.Light_Specular.Set(L.Specular);
   end Send_Render_Data;

   ------------
   -- Update --
   ------------

   procedure Update(E : in out Entity) is
   begin
      E.Position := E.Position + (E.Velocity * Globals.Seconds_Per_Frame);
   end Update;

   ------------------------
   -- Accelerate_Towards --
   ------------------------

   procedure Accelerate_Towards(E              : in out Entity;
                                Target         : in     Position_Type;
                                Time_To_Arrive : in     Time_Span)
   is
      Diff : constant Velocity_Type := ((Target - E.Position) / Time_To_Arrive);
   begin
      pragma Assert((for all X of Diff => X'Valid),
                    "Accelerate_Towards was given an unreasonable time span.");
      E.Velocity := (E.Velocity + Diff);
   end Accelerate_Towards;

   ----------------------
   -- Entities_Collide --
   ----------------------

   function Entities_Collide(A, B : in Entity) return Boolean is

      A_Scaled_Z : constant Single := A.Scale * (A.RVAO.Dimensions(Z) / 2.0);
      B_Scaled_Z : constant Single := B.Scale * (B.RVAO.Dimensions(Z) / 2.0);

      A_Near     : constant Single := A.Position(Z) - A_Scaled_Z;
      A_Far      : constant Single := A.Position(Z) + A_Scaled_Z;
      B_Near     : constant Single := B.Position(Z) - B_Scaled_Z;
      B_Far      : constant Single := B.Position(Z) + B_Scaled_Z;

      type A_Range is new Single range A_Near .. A_Far;
      type B_Range is new Single range B_Near .. B_Far;

   begin

      -- If they're not in the same lane, they don't collide.
      if A.Position(X) /= B.Position(X) then return False; end if;
      -- Should be on the same height.
      if A.Position(Y) /= B.Position(Y) then return False; end if;

      -- NOTE(wtok, 2018-07-11): This only makes sense when we only
      --  care about collisions along the Z axis, i.e. two objects in
      --  the same lane.
      --
      -- In principle, this scenario doesn't get caught by this test:
      --
      --      +---+          ^
      --      |   |          |
      --   +---------+       |
      --   |  |   |  |       |
      --   +---------+       |
      --      |   |          |
      --      +---+       Z-axis
      --
      -- But since we only care about the Z-position, this equation
      --  considers these two to collide. To us it looks like this:
      --
      --    +
      --    |
      --    +
      --    |
      --    +
      --    |
      --    +
      --
      -- So it's worth noting that in any other context, this function
      --  will probably *not* do what you think it does.

      return (         A_Near in B_Near .. B_Far
               or else A_Far  in B_Near .. B_Far
               or else B_Near in A_Near .. A_Far
               or else B_Far  in A_Near .. A_Far);

   end Entities_Collide;

   -------------------
   -- Is_Off_Screen --
   -------------------

   function Is_Off_Screen(E : in Entity) return Boolean is
      Dim      : constant Volume3D_Type := E.Scaled_Dimensions;
      Z_Extent : constant Single        := (E.Position(Z) - Dim(Z));
   begin
      return Z_Extent > Globals.Camera_Position(Z);
   end Is_Off_Screen;

   --------------------------
   -- Disable_If_Invisible --
   --------------------------

   procedure Disable_If_Invisible(E : in out Entity) is
   begin
      if E.Is_Live and then E.Is_Off_Screen then
         E.Is_Live := False;
      end if;
   end Disable_If_Invisible;

   --------------
   -- Gameplay --
   --------------


   --------------------
   -- Spawn_Obstacle --
   --------------------

   procedure Spawn_Obstacle(L : in Lane) is
      Pos : constant Position_Type :=
         (X => Lane_X(L),
          Y => 0.0,
          Z => -10.0 * Renderables.Prefab_RVAOs.Car.Dimensions(Z));

      First_Free_Index : Positive;
   begin

		-- If all slots for barrels are filled, this function does nothing.
   	if (for all B of Barrels => B.Is_Live) then
   		return;
   	end if;

   	for Idx in Barrels'Range loop
   		if not Barrels(Idx).Is_Live then
   			First_Free_Index := Idx;
   			exit;
   		end if;
		end loop;

      Barrels(First_Free_Index) :=
      	(RVAO     => Renderables.Prefab_RVAOs.Barrel,
          Position => Pos,
          Velocity => (others => 0.0),
          Rotation => Radians(0),
          Scale    => 1.0,
          Is_Live  => True);
   end Spawn_Obstacle;

   ------------------
   -- Change_Lanes --
   ------------------

   procedure Change_Lanes(E         : in out Entity;
                          Direction : in     Lane_Change_Direction)
   is
      Lane_X : constant array (Lane) of Single
         := (1 => +1.0, 2 => 0.0, 3 => -1.0);
      Ent_X  : constant Single := E.Position(X);

      Source_Lane : Lane;
      Target_Lane : Lane;

      use Ada.Text_IO;
   begin
      if Ent_X = Lane_X(1) or else Ent_X > Lane_X(2) then
         Source_Lane := 1;
         Target_Lane := (if Direction = Left then 1 else 2);
      elsif Ent_X = Lane_X(2) then
         Source_Lane := 2;
         Target_Lane := (if Direction = Left then 1 else 3);
      elsif Ent_X = Lane_X(3) or else Ent_X < Lane_X(2) then
         Source_Lane := 3;
         Target_Lane := (if Direction = Left then 2 else 3);
      end if;

      Put_Line("From Lane " & Lane'Image(Source_Lane)
                  & " to Lane " & Lane'Image(Target_Lane));

      E.Position(X) := Lane_X(Target_Lane);
   end Change_Lanes;

end Cargame.Entity;
