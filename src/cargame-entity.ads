with Ada.Containers.Vectors;
with Ada.Real_Time;

with GL.Types;
with GL.Types.Colors;

with Cargame.Types;
with Cargame.Obj_Parser;
with Cargame.Renderables;
with Cargame.Vectors;

-- Gameplay stuff is going in here for the moment because I keep running into
--  circular dependencies.

package Cargame.Entity is

   use Cargame.Renderables;
   use Cargame.Types;
   use Cargame.Obj_Parser;
   use GL.Types, GL.Types.Singles, GL.Types.Colors;

	--------------
   -- Gameplay --
	--------------

	-- Lanes --

   Num_Lanes : constant Integer := 3;

   type Lane is new Integer range 1 .. Num_Lanes;

   Lane_X : constant array (Lane) of Single :=
      (1 => -1.0, 2 => 0.0, 3 => +1.0);

   -- Obstacles --

   procedure Spawn_Obstacle(L : in Lane);


   type Lane_Change_Direction is (Left, Right);

   ------------
   -- Entity --
   ------------

   type Entity is tagged record
      RVAO     : Renderable_VAO;
      Position : Position_Type  := (0.0, 0.0, 0.0);
      Velocity : Velocity_Type  := (0.0, 0.0, 0.0);
      Rotation : Radians        := 0.0;
      Scale    : Single         := 1.0;
      Is_Live  : Boolean        := False;
   end record;

   procedure Update  (E : in out Entity) with Inline;

   procedure Render  (E : in     Entity)
      with Pre => (E.Is_Live
      					and then Is_Actually_Renderable(E.RVAO));

   procedure Move    (E : in out Entity; Distance : in Distance_Type)
   	  with Inline,
   	  	    Global => null,
   	  	    Post   => (E.Position = (E'Old.Position + Distance));

   procedure Place_At(E : in out Entity; Position : in Position_Type)
      with Inline,
           Global => null,
           Post   => (E.Position = Position);

   procedure Rotate  (E        : in out Entity;
                      Angle    : in     Radians) with Inline;
   procedure Rotate  (E        : in out Entity;
                      Angle    : in     Degrees) with Inline;

   use Ada.Real_Time;

   procedure Accelerate_Towards(E              : in out Entity;
                                Target         : in     Position_Type;
                                Time_To_Arrive : in     Time_Span)
      with Inline,
      	  Global => null,
      	  Pre    => Time_To_Arrive /= Seconds(0);
      	  -- FIXME: Double-check this math
      	  -- Post  => (Target = (E.Position + E.Velocity * (Time_To_Arrive / Seconds(1))));

   function Entities_Collide(A, B : in Entity) return Boolean;
   -- So we can write A.Collides_With(B)
   function Collides_With   (A, B : in Entity) return Boolean
   	renames Entities_Collide;

   function Is_Off_Screen(E : in Entity) return Boolean;

   procedure Change_Lanes(E         : in out Entity;
   						     Direction : in     Lane_Change_Direction);

   procedure Disable_If_Invisible(E : in out Entity);

   function Scaled_Dimensions(E : in Entity) return Volume3D_Type is
   	(E.RVAO.Dimensions * E.Scale) with Inline;

   package Entity_Vectors is
      new Ada.Containers.Vectors(Positive, Entity);
   subtype Vector_Of_Entity is Entity_Vectors.Vector;

   type Array_Of_Entity is
     array (Positive range <>) of aliased Entity;

   function Entity_Vector_With(Initial_Elements : in Array_Of_Entity)
                              return Vector_Of_Entity;

   function Append_Default_Entity(V : in out Vector_Of_Entity)
                                 return Natural;

   -- All entities in the game, I guess.
   The_Player  : Entity;
	Max_Barrels : constant Positive := 5;
   Barrels     : Array_Of_Entity(1 .. Max_Barrels);


   type Light is tagged record
      Position  : Position_Type  := (0.0, 0.0, 0.0);
      Velocity  : Velocity_Type  := (0.0, 0.0, 0.0);
      Rotation  : Radians        := 0.0;
      Ambient, Diffuse, Specular : Color;
   end record;

   procedure Send_Render_Data(L : in Light);

end Cargame.Entity;
