with GL.Uniforms;
with Ada.Text_IO;
with Cargame.Types;

with Cargame.Uniforms;
with Cargame.Globals;  use Cargame.Globals;
with Cargame.Types;

with Glfw;
with Glfw.Errors;

with Cargame.Entity;

package body Cargame.Globals is

    --------------------
    -- Glfw callbacks --
    --------------------

    procedure Key_Changed(Object   : not null access Main_Window_Type;
                          Key      : Glfw.Input.Keys.Key;
                          Scancode : Glfw.Input.Keys.Scancode;
                          Action   : Glfw.Input.Keys.Action;
                          Mods     : Glfw.Input.Keys.Modifiers)
    is
        use GL.Uniforms;
        use Glfw.Input.Keys; -- NOTE: X,Y,Z will conflict with vector members.
    begin

        if Action = Release then
            -- I don't care about release events.
            return;
        end if;

        case Key is
           when Q      => Main_Window.Set_Should_Close(True);
           when Left   => Entity.The_Player.Change_Lanes(Entity.Left);
           when Right  => Entity.The_Player.Change_Lanes(Entity.Right);
           when others => null;
        end case;
    end Key_Changed;

    procedure Size_Changed(Object : not null access Main_Window_Type;
                           Width, Height : Natural)
    is
        use Ada.Text_IO;
    begin
        Put_Line("New window size: "
                     & Natural'Image(Width) & " x " & Natural'Image(Height)
                     & " (aspect ratio: " & Single'Image(Aspect_Ratio) & ")");
        Main_Window_Width  := Glfw.Size(Width);
        Main_Window_Height := Glfw.Size(Height);
        Uniforms.Projection.Set
          (Types.Perspective_Matrix(View_Angle   => Types.Degrees(90.0),
                                    Aspect_Ratio => Globals.Aspect_Ratio,
                                    Near         => Globals.Near_Plane,
                                    Far          => Globals.Far_Plane));

    end Size_Changed;

    procedure Mouse_Position_Changed(Object : not null access Main_Window_Type;
                                     X, Y   : Glfw.Input.Mouse.Coordinate)
    is
    begin
        Cargame.Globals.Mouse.X := X;
        Cargame.Globals.Mouse.Y := Y;
        --  Cargame.Globals.Object_Rotation := Degrees(X);
    end Mouse_Position_Changed;

    procedure Mouse_Button_Changed(Object : not null access Main_Window_Type;
                                   Button : Glfw.Input.Mouse.Button;
                                   State  : Glfw.Input.Button_State;
                                   Mods   : Glfw.Input.Keys.Modifiers)
    is
        use Glfw.Input.Mouse;
        use Cargame.Globals.Mouse;
    begin
        if Button = Left_Button then
            Left_Button_State := State;
        elsif Button = Right_Button then
            Right_Button_State := State;
        end if;
    end Mouse_Button_Changed;

end Cargame.Globals;
