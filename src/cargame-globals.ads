with Ada.Real_Time;        use Ada.Real_Time;
with GL;                   use GL;
with GL.Objects.Programs;
with GL.Types.Colors;      use GL.Types.Colors;
with GL.Types;             use GL.Types; use GL.Types.Singles;
with Glfw.Input;           use Glfw.Input;
with Glfw.Input.Keys;
with Glfw.Input.Mouse;
with Glfw.Windows;         use Glfw.Windows;

--
-- NOTE(wtok):
--
-- This package was originally intended to just contain globals
--  willy-nilly as needed. That turns out to be a bit complicated due
--  to circular unit dependencies, and this is one of the only
--  packages in the program that doesn't depend on any other Cargame
--  modules. That means this should probably be called
--  "Program_Globals", since it can't really contain any game logic.
--

--
-- TODO: I think a really sensible thing to do might be to split this into
--  Cargame.Globals and Cargame.Constants.
--

package Cargame.Globals is

   -----------------
   -- Main_Window --
   -----------------

   type Main_Window_Type is new Window with null record;

   -- Overrides

   use Glfw.Windows.Callbacks;
   use Glfw.Input;

   overriding procedure Key_Changed            (Object   : not null access Main_Window_Type;
                                                Key      : Keys.Key;
                                                Scancode : Keys.Scancode;
                                                Action   : Keys.Action;
                                                Mods     : Keys.Modifiers);
   overriding procedure Size_Changed           (Object   : not null access Main_Window_Type;
                                                Width    : Natural;
                                                Height   : Natural);
   overriding procedure Mouse_Position_Changed (Object   : not null access Main_Window_Type;
                                                X, Y     : Mouse.Coordinate);
   overriding procedure Mouse_Button_Changed   (Object   : not null access Main_Window_Type;
                                                Button   : Mouse.Button;
                                                State    : Button_State;
                                                Mods     : Keys.Modifiers);

   -------------------------
   -- Program information --
   -------------------------

   Program_Epoch          : Time               := Clock;
   Next_Frame_Time        : Time               := Clock;
   Target_FPS             : constant Positive  := 120;
   Seconds_Per_Frame      : constant Single    := (Single(1.0) / Single(Target_FPS));
   Frame_Interval         : constant Time_Span := (Seconds(1) / Target_FPS);

   Next_Input_Poll_Time   : Time               := Clock;
   Input_Polls_Per_Second : constant Positive  := (Target_FPS / 2);
   Input_Poll_Interval    : constant Time_Span := (Seconds(1) / Input_Polls_Per_Second);

   -- Rendering state
   Main_Window_Object : aliased Main_Window_Type;
   Main_Window        : constant not null access Main_Window_Type 
      := Main_Window_Object'Access;

   Main_Window_Width  : Glfw.Size := 1280;
   Main_Window_Height : Glfw.Size := 720;

   function Aspect_Ratio return Single is
      (Single(Main_Window_Width) / Single(Main_Window_Height));

   Background_Colour : constant Color               := (others => 0.1);

   --  Vertical_FoV  : Degrees                      := Degrees(60.0);
   Vertical_FoV      : Single                       := 60.0;
   GL_Program        : GL.Objects.Programs.Program;
   Near_Plane        : constant                     := 1.0;
   Far_Plane         : constant                     := 5.0;

   Camera_Position   : Vector3                      := (0.0, 0.0, -2.0); 

   Diffuse_Map_ID  : constant Int := 0;
   Specular_Map_ID : constant Int := 1;

   package Mouse is
      Left_Button_State  : Button_State;
      Right_Button_State : Button_State;
      X, Y : Glfw.Input.Mouse.Coordinate;
   end Mouse;

   -- Unit of time, so we can write stuff like
   --  "Input_Poll_Interval := Frames(5);"
   function Frames(Num : in Natural) return Time_Span is (Num * Frame_Interval);

end Cargame.Globals;
