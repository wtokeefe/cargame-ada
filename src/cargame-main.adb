with Ada.Command_Line;
with Ada.Directories;
with Ada.Real_Time;            use Ada.Real_Time;
with Ada.Text_IO;              use Ada.Text_IO;

with GL;                       use GL;
with GL.Buffers;               use GL.Buffers;
with GL.Files;
with GL.Objects.Programs;      use GL.Objects.Programs;
with GL.Objects.Shaders;       use GL.Objects.Shaders;
with GL.Objects.Textures;
with GL.Toggles;
with GL.Types;                 use GL.Types; use GL.Types.Singles;
with GL.Uniforms;              use GL.Uniforms;

with Glfw;
with Glfw.Input;               use Glfw.Input;
with Glfw.Windows;             use Glfw.Windows;
with Glfw.Windows.Context;     use Glfw.Windows.Context;
with Glfw.Windows.Hints;       use Glfw.Windows.Hints;

with Cargame.Globals;          use Cargame.Globals;
with Cargame.Renderables;      use Cargame.Renderables;
with Cargame.Types;            use Cargame.Types;
with Cargame.Uniforms;
with Cargame.Entity;           use Cargame.Entity;
with Cargame.Util;
with Cargame.Terrain;
with Cargame.Texture_Loader;

procedure Cargame.Main is

   A_Light  : constant Light := Light'(Position => (0.0, 0.0, 0.0),
                                       Velocity => (others => 0.0),
                                       Rotation => Radians(0.0),
                                       Ambient  => (1.0, 1.0, 1.0, 1.0),
                                       Diffuse  => (1.0, 1.0, 1.0, 1.0),
                                       Specular => (1.0, 1.0, 1.0, 1.0));

   Terrains : Array_Of_Entity(1 .. 3);

   Frame_T0 : Time;
   Frame_T1 : Time;

   ----------------
   -- The player --
   ----------------

   Default_Player_Velocity : constant Velocity_Type :=
      (Z => 0.01, others => 0.0);

   Player_Velocity_Delta   : constant Velocity_Type :=
      (Z => 0.001, others => 0.0);

   Top_Score : Single := 0.0;

   function Player_Velocity return Velocity_Type is (The_Player.Velocity);

   procedure Maybe_Update_Top_Score is
   begin
   	if The_Player.Velocity(Z) > Top_Score then
   		Top_Score := The_Player.Velocity(Z);
		end if;
	end Maybe_Update_Top_Score;

   procedure Increase_Player_Velocity is
   begin
      The_Player.Velocity := (The_Player.Velocity + Player_Velocity_Delta);
   end Increase_Player_Velocity;

   procedure Reset_Player_Velocity is
   begin
      The_Player.Velocity := Default_Player_Velocity;
   end Reset_Player_Velocity;

begin

   Ada.Directories.Set_Directory
       (Ada.Directories.Containing_Directory(Ada.Command_Line.Command_Name));

   ----------------------------
   -- Set up GLFW and OpenGL --
   ----------------------------

   Util.Got_Here;

   Glfw.Init; -- Initialises both GLFW and OpenGL.

   Set_Minimum_OpenGL_Version(Major => 3, Minor => 3);
   Set_Forward_Compat(True);
   Set_Profile(Core_Profile);

   Init(Object => Globals.Main_Window,
        Title  => "Skeltal Racing",
        Width  => Globals.Main_Window_Width,
        Height => Globals.Main_Window_Height);

   pragma Assert(Main_Window.Initialized,
                 "Failed to initialise main window.");

   Make_Current(Main_Window);
   Set_Swap_Interval(0);

   Set_Color_Clear_Value(Globals.Background_Colour);

   -- Enable callbacks
   Main_Window.Enable_Callback(Glfw.Windows.Callbacks.Key);
   Main_Window.Enable_Callback(Glfw.Windows.Callbacks.Size);
   Main_Window.Enable_Callback(Glfw.Windows.Callbacks.Mouse_Position);
   Main_Window.Enable_Callback(Glfw.Windows.Callbacks.Mouse_Button);

   Util.Got_Here("Setting Up Shaders.");

   ---------------------------------
   -- Set up GL_Program (shaders) --
   ---------------------------------

   declare
      Vertex_Shader   : Shader(Kind => GL.Objects.Shaders.Vertex_Shader);
      Fragment_Shader : Shader(Kind => GL.Objects.Shaders.Fragment_Shader);

      Vertex_Path   : constant String := "../src/shaders/vert.glsl";
      Fragment_Path : constant String := "../src/shaders/frag.glsl";
      use Ada.Directories;
   begin
      pragma Assert(Exists(Vertex_Path),   "Couldn't find vertex shader.");
      pragma Assert(Exists(Fragment_Path), "Couldn't find fragment shader.");

      Initialize_ID(GL_Program);
      Initialize_ID(Vertex_Shader);
      Initialize_ID(Fragment_Shader);

      GL.Files.Load_Shader_Source_From_File(Vertex_Shader, Vertex_Path);
      GL.Files.Load_Shader_Source_From_File(Fragment_Shader, Fragment_Path);

      Compile(Vertex_Shader);
      Compile(Fragment_Shader);

      if not Vertex_Shader.Compile_Status then
         Put_Line("Log: " & Vertex_Shader.Info_Log);
         pragma Assert(False, "Failed to compile Vertex shader.");
      end if;

      if not Fragment_Shader.Compile_Status then
         Put_Line("Log: " & Fragment_Shader.Info_Log);
         pragma Assert(False, "Failed to compile Fragment shader.");
      end if;

      GL_Program.Attach(Vertex_Shader);
      GL_Program.Attach(Fragment_Shader);
      GL_Program.Link;

      if not GL_Program.Link_Status then
         Put_Line("Log: " & GL_Program.Info_Log);
         pragma Assert(False, "Failed to link shaders.");
      end if;
   end; -- shader setup

   Util.Got_Here("Shaders done.");

   GL_Program.Use_Program;
   GL.Toggles.Enable(GL.Toggles.Depth_Test);

   Util.Got_Here;

   -------------------------
   -- Initialise Uniforms --
   -------------------------

   Uniforms.Projection.Initialise_With_Value
     (GL_Program, Val => Perspective_Matrix(View_Angle   => Degrees(90.0),
                                            Aspect_Ratio => Globals.Aspect_Ratio,
                                            Near         => Globals.Near_Plane,
                                            Far          => Globals.Far_Plane));
   Uniforms.Camera_Transform.Initialise_With_Value
     (GL_Program, Val => Look_At(Camera_Position => (0.0, 2.0, -2.0),
                                 Target_Position => (others => 0.0),
                                 Up              => (Y => 1.0, others => 0.0)));
   Uniforms.Object_Transform.Initialise_With_Value(GL_Program, Identity4);
   Uniforms.CamObj_Transform.Initialise_With_Value(GL_Program, Identity4);
   Uniforms.Normal_Transform.Initialise(GL_Program);
   Uniforms.Diffuse_Map.Initialise_With_Value(GL_Program, Globals.Diffuse_Map_ID);
   Uniforms.Specular_Map.Initialise_With_Value(GL_Program, Globals.Specular_Map_ID);
   Uniforms.Material_Ambient.Initialise(GL_Program);
   Uniforms.Material_Shininess.Initialise(GL_Program);
   Uniforms.Light_Position.Initialise(GL_Program);
   Uniforms.Light_Ambient.Initialise(GL_Program);
   Uniforms.Light_Diffuse.Initialise(GL_Program);
   Uniforms.Light_Specular.Initialise(GL_Program);

   -- Send Light data
   A_Light.Send_Render_Data;

   -------------------------
   -- Initialise entities --
   -------------------------

   declare
      Car    : constant Renderable_VAO := Create_Renderable_VAO("../src/models/car-n.obj");
      Barrel : constant Renderable_VAO := Create_Renderable_VAO("../src/models/Barrel02.obj");
   begin
      -- Used in future
      Renderables.Prefab_RVAOs.Car    := Car;
      Renderables.Prefab_RVAOs.Barrel := Barrel;

      The_Player := 
          (RVAO     => Car,
           Position => (0.0, 0.0, 0.0),
           Velocity => (others => 0.0),
           Scale    => 0.2,
           Rotation => 0.0,
           Is_Live  => True);

      Entity.Barrels :=
      	(others => (RVAO     => Barrel,
           				Position => (0.0, 0.0, 10.0),
           				Velocity => (others => 0.0),
           				Scale    => 10.0,
           				Rotation => 0.0,
           				Is_Live  => True));
   end;

   for E of Entity.Barrels loop
		E.Accelerate_Towards(Target         => (0.0, 0.0, 0.0),
                           Time_To_Arrive => Seconds(5));
	end loop;

   Put_Line("Entities loaded and initialised.");

	--------------------------
   -- Set default material --
   --------------------------

	declare
		Default_Texture : GL.Objects.Textures.Texture
			:= Texture_Loader.Load_Texture("../src/models/default_texture.png");
		pragma Assert(Default_Texture.Raw_ID /= 0);
	begin
      Default_Material :=
      	(Name             => To_Material_Name("Default material"),
      	 Diffuse_Texture  => Default_Texture,
      	 Specular_Texture => Default_Texture,
      	 others           => <>);
   end;

	------------------------
   -- Initialise terrain --
	------------------------

	Terrains := (others => Terrain.Generate_Terrain_Segment);

	for T of Terrains loop
		declare
			Z_Dim : constant Single := T.Scaled_Dimensions(Z);
		begin
   		T.Position(Z) := T.Position(Z) - Z_Dim;
		end;
	end loop;

   --------------------
   -- Main game loop --
   --------------------

   Util.Got_Here("Before main loop.");

   loop

      -------------------
      -- Set deadlines --
      -------------------

      -- Evaluate Clock as the first thing in the frame.
      Frame_T0 := Clock;

      Next_Frame_Time := Frame_T0 + Frame_Interval;

      if Frame_T0 > Globals.Next_Input_Poll_Time then
         Glfw.Input.Poll_Events;
         Globals.Next_Input_Poll_Time
           := Globals.Next_Input_Poll_Time + Globals.Input_Poll_Interval;
      end if;

      -----------
      -- Frame --
      -----------

      exit when Main_Window.Should_Close;

      Clear(Buffer_Bits'(Depth => True, Color => True, others => <>));

		for B of Entity.Barrels loop
			B.Disable_If_Invisible;
		end loop;

      Increase_Player_Velocity;
      Maybe_Update_Top_Score;

      -- Place procedural terrain

      Uniforms.Procedural_Terrain.Set(1); -- True.

      declare
         Back_Of_Queue_Position : constant Position_Type :=
            (Z => -10.0, others => 0.0);
      begin
         for T of Terrains loop
            if T.Is_Off_Screen then
            	Put_Line("Terrain went off screen.");
               T.Position := Back_Of_Queue_Position;
            else
               T.Position := T.Position + Player_Velocity;
            end if;

				Put_Line("Rendering terrain at pos " & Image(Vector3(T.Position)));
				T.Render;
         end loop;
      end;

      Uniforms.Procedural_Terrain.Set(0); -- False

		-- Update the player
		Increase_Player_Velocity;
		--  The_Player.Update;
		The_Player.Render;

      -- Update entities
      for Barrel of Barrels loop
         if Barrel.Is_Live then
            Barrel.Update;
            Barrel.Render;

            if Barrel.Collides_With(The_Player) then
            	Reset_Player_Velocity;
            	Maybe_Update_Top_Score;
            	Barrel.Is_Live := False;
            	Put_Line("Ouch!");
         	end if;

         end if;
      end loop;

      GL.Flush;
      Swap_Buffers(Main_Window);

      --------------------------
      -- Post-frame analytics --
      --------------------------

      Frame_T1 := Clock;

      if Frame_T1 > Next_Frame_Time then
         Put_Line("Missed frame duration target by "
                    & Util.Time_Span_Image(Frame_T1 - Next_Frame_Time));
      end if;

      -- Wait until frame deadline, if necessary.
      delay until Next_Frame_Time;

   end loop; -- Main loop

   Destroy(Main_Window);
   Put_Line("Thanks for playing!");

   Glfw.Shutdown;

end Cargame.Main;
