with Ada.Directories;        use Ada.Directories;
with Ada.Strings.Maps;       use Ada.Strings.Maps;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Text_IO.Unbounded_IO;
with Ada.Characters.Latin_1;

with Cargame.Obj_Parser;     use Cargame.Obj_Parser;
with Cargame.Util;           use Cargame.Util;
with Cargame.Texture_Loader; use Cargame.Texture_Loader;

-- Input example:
--
--  newmtl p206-tp.bmp_p206-tp.j_p206-tp.jpg
--  Ns 90.196078
--  Ka 0.000000 0.000000 0.000000
--  Kd 0.409600 0.409600 0.409600
--  Ks 0.125000 0.125000 0.125000
--  Ni 1.000000
--  d 1.000000
--  illum 2
--  map_Kd p206-tp.jpg

package body Cargame.Obj_Parser.Mtl_Parser is

    package UBIO renames Ada.Text_IO.Unbounded_IO;
    package UIO is new Ada.Text_IO.Modular_IO(UInt);
    package FIO is new Ada.Text_IO.Float_IO(Single);

    Valid_Mtl_Character_Set : constant Character_Set
        := To_Set(Character_Ranges'((Low => '0', High => '9'),
                                    (Low => 'a', High => 'z'),
                                    (Low => 'A', High => 'Z'),
                                    (others => '.'),
                                    (others => '-'),
                                    (others => '_')));

    function Parse_Mtl(Mtl_File_Path : in String) return Vector_Of_Material is
        Mtl_File         : File_Type;
        Line             : Unbounded_String;
        Split_Line       : UB_String_Vector;
        Output_Materials : Vector_Of_Material;
        Current_Material : Material;
        First_Material   : Boolean := True;
    begin

        Open(File => Mtl_File,
             Mode => In_File,
             Name => Mtl_File_Path);

        while not End_Of_File(Mtl_File) loop

            Line := UBIO.Get_Line(Mtl_File);

            -- Skip empty lines and comments
            while not End_Of_File(Mtl_File)
                and then (Length(Line) = 0
                              or else Element(Line, 1) = '#'
                              or else Element(Line, 1) = Ada.Characters.Latin_1.CR)
            loop
                Line := UBIO.Get_Line(Mtl_File);
            end loop;

            exit when End_Of_File(Mtl_File);

            Split_Line := Split_Into_Tokens(Line, Valid_Mtl_Character_Set);

            declare

                type Mtl_Token is (Newmtl, Map_Kd, Illum, Ni, Ns, Ks, Kd, Ka, Ke, D);
                Token : constant Mtl_Token := Mtl_Token'Value(To_String(Split_Line(1)));

                function Get_Single(S : in Unbounded_String) return Single is
                    Str : constant String := To_String(S);
                    Got : Single;
                    Unused : Positive;
                begin
                    FIO.Get(Str, Got, Unused);
                    return Got;
                end Get_Single;

            begin

                case Token is
                    when Ka => Current_Material.Ambient_Light  := Get_Vector3(Split_Line);
                    when Kd => Current_Material.Diffuse_Light  := Get_Vector3(Split_Line);
                    when Ks => Current_Material.Specular_Light := Get_Vector3(Split_Line);
                    when Ns => Current_Material.Shininess      := Get_Single(Split_Line(2));

                    when Map_Kd =>
                        -- Diffuse texture. Next token should be path to an image file.

                        pragma Assert(Ada.Directories.Exists(To_String(Split_Line(2))),
                                      "Mtl file specified a texture file that I can't find.");

                        Initialize_ID(Current_Material.Diffuse_Texture);
                        Current_Material.Diffuse_Texture := Load_Texture(To_String(Split_Line(2)));

                        Initialize_ID(Current_Material.Specular_Texture);
                        Current_Material.Specular_Texture := Load_Texture("default_texture.png");

                    when Newmtl =>
                        -- Material name. Next token should be a string without spaces.

                        if not First_Material then
                           Output_Materials.Append(Current_Material);
                        end if;

                        First_Material := False;
                        Current_Material := (Name   => To_Material_Name(Split_Line(2)),
                                             others => <>);

                    when Ni    => null; -- TODO: Index of refraction
                    when Illum => null; -- TODO: Illumination model
                    when Ke    => null; -- TODO: ???
                    when D     => null; -- TODO: Dissolve
                end case;

            end;

        end loop;

        -- We append materials when we encounter the next one, which means the last material won't
        --  be appended. Do that here.
        Output_Materials.Append(Current_Material);

        Put_Line("Finished parsing Mtl.");

        return Output_Materials;
    end Parse_Mtl;

end Cargame.Obj_Parser.Mtl_Parser;
