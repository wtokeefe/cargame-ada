with Ada.Directories;               use Ada.Directories;
with Ada.Strings.Maps;              use Ada.Strings.Maps;
with Ada.Strings;                   use Ada.Strings;
with Ada.Text_IO;                   use Ada.Text_IO;
with Ada.Text_IO.Unbounded_IO;
with GL;                            use GL;
with Cargame.Obj_Parser.Mtl_Parser; use Cargame.Obj_Parser.Mtl_Parser;

-- TODO(wtok, 2018-02-28): Aggressively apply semantic compression. Rearrange data. Delete stuff.

package body Cargame.Obj_Parser is

    package UIO is new Ada.Text_IO.Modular_IO(UInt);
    package FIO is new Ada.Text_IO.Float_IO(Single);
    package UBIO renames Ada.Text_IO.Unbounded_IO;

    procedure Get_Face(Split_Line                      : in     UB_String_Vector;
                       Output_VI, Output_NI, Output_TI : in out Vector_Of_UInt)
        is separate
        with Pre => (Split_Line.Length >= 4 and then Split_Line(1) = "f"),
        -- Face must at least specify vertices.
        Post => ((Output_VI.Length >  Output_VI'Old.Length)
                 and then (Output_NI.Length >= Output_NI'Old.Length)
                 and then (Output_TI.Length >= Output_TI'Old.Length)
                 and then (if Output_NI.Length /= 0
                               then Output_NI.Length = Output_VI.Length)
                 and then (if Output_TI.Length /= 0
                               then Output_TI.Length = Output_VI.Length));

    function Get_Vector3(Split_Line : in UB_String_Vector) return Vector3 is
        V      : Vector3;
        Unused : Positive;
    begin
        FIO.Get(To_String(Split_Line(2)), V(X), Unused);
        FIO.Get(To_String(Split_Line(3)), V(Y), Unused);
        FIO.Get(To_String(Split_Line(4)), V(Z), Unused);
        return V;
    end Get_Vector3;

    function Get_Vector2(Split_Line : in UB_String_Vector) return Vector2 is
        V      : Vector2;
        Unused : Positive;
    begin
        FIO.Get(To_String(Split_Line(2)), V(X), Unused);
        FIO.Get(To_String(Split_Line(3)), V(Y), Unused);
        return V;
    end Get_Vector2;

    --------------------
    -- Character sets --
    --------------------

    Valid_Obj_Character_Set : constant Character_Set
        := To_Set(Character_Ranges'((Low => '0', High => '9'),
                                    (Low => 'a', High => 'z'),
                                    (Low => 'A', High => 'Z'),
                                    (others => '-'),
                                    (others => '+'),
                                    (others => '.'),
                                    (others => '/')));

    function Parse(File_Path        : in     String) return Obj_Data is
        Model : Obj_Data; -- Return value

        use Material_Names;
        type Obj_Token is (V, VN, VT, MtlLib, UseMtl, S, G, F, O);
        Token                 : Obj_Token;
        Line                  : Unbounded_String;
        Obj_File              : File_Type;
        Split_Line            : UB_String_Vector;
        Current_Material_Name : Material_Name;
        Original_Directory    : constant String := Current_Directory;
    begin

        Set_Directory(Containing_Directory(File_Path));

        Open(File => Obj_File,
             Mode => In_File,
             Name => Simple_Name(File_Path));

        while not End_Of_File(Obj_File) loop
            Line := UBIO.Get_Line(Obj_File);

            -- Skip empty lines and comments
            while not End_Of_File(Obj_File)
                and then (Length(Line) = 0 or else Element(Line, 1) = '#')
                loop
                    Line := UBIO.Get_Line(Obj_File);
            end loop;

            exit when End_Of_File(Obj_File);

            Split_Line := Split_Into_Tokens(Line, Valid_Obj_Character_Set);
            Token      := Obj_Token'Value(To_String(Split_Line(1)));

            case Token is

                when VN => Model.Unique_Normals.Append
                   (Get_Vector3(Split_Line));

                when VT => Model.Unique_TexCrds.Append
                   (Get_Vector2(Split_Line));

                when V => Model.Unique_Vertices.Append
                   (Get_Vector3(Split_Line));

                when F => Get_Face
                   (Split_Line => Split_Line,
                    Output_VI  => Model.Vertex_Indices,
                    Output_NI  => Model.Normal_Indices,
                    Output_TI  => Model.TexCrd_Indices);

                when MtlLib =>
                    Put_Line("Opening MTL file " & To_String(Split_Line(2)));
                    Model.Materials := Parse_Mtl(To_String(Split_Line(2)));

                when UseMtl =>

                    declare
                        Got_Name : constant Material_Name
                          := To_Material_Name(Split_Line(2));

                        -- It seems that 'usemtl' is often used with
                        --  just enough of the actual material name to
                        --  be unambiguous. So we can't just compare
                        --  (Got_Name = Have_Name), we have to compare
                        --  them up to the length of the shortest
                        --  string.
                        function Names_Match(A, B : in Material_Name) return Boolean
                            is (if (Length(A) = 0 or else Length(B) = 0)
                                    then False
                                    else (if (Length(A) > Length(B))
                                              then (Head(A, Length(B)) = B)
                                              else (Head(B, Length(A)) = A)));

                    begin

                        pragma Assert(Names_Match(To_Material_Name("ABC"), To_Material_Name("AB")),
                                      "Unit test failed for Names_Match.");

                        pragma Assert(Names_Match(To_Material_Name("AB"), To_Material_Name("ABC")),
                                      "Unit test failed for Names_Match.");

                        pragma Assert(Model.Materials.Length /= 0,
                                      "Got usemtl, but we don't have any materials!");

                        pragma Assert((for some M of Model.Materials => Names_Match(M.Name, Got_Name)),
                                      "Got a usemtl line for Material["
                                          & To_String(Got_Name)
                                          & "], which I can't find!");

                        -- This has to be two separate loops, since we
                        --  can't assume anything about the sorting of
                        --  the Materials vector. Particularly, if we
                        --  find Got_Name before we find
                        --  Current_Material_Name, then we'll set
                        --  Current_Material_Name to Got_Name and
                        --  never update the Final_Index of the
                        --  previous material.

                        for Mtl of Model.Materials loop
                            if Names_Match(Mtl.Name, Current_Material_Name) then
                                -- We're at a usemtl line, so we're
                                --  done with the current
                                --  material. Set its num indices.
                                Mtl.Final_Index := Integer
                                  (Model.Vertex_Indices.Length - 1);
                            end if;
                        end loop;

                        for Mtl of Model.Materials loop
                            if Names_Match(Mtl.Name, Got_Name) then
                                Mtl.First_Index := Integer
                                  (Model.Vertex_Indices.Length);
                                Current_Material_Name := Mtl.Name;
                            end if;
                        end loop;

                    end;
                when G => null;
                when O => null; -- TODO(wtok, 2018-03-29)
                when S => null; -- TODO(wtok, 2018-03-29)
            end case;

        end loop;

        Close(Obj_File);

        Set_Directory(Original_Directory);

        -- We set Final_Index when we encounter the next material, so
        --  that won't happen for the last material. Do that here.
        pragma Assert((for some M of Model.Materials => M.Final_Index = -1),
                      "All materials have Final_Index set. That's not supposed to happen.");
        for M of Model.Materials loop
            if (M.Final_Index = -1) then
                M.Final_Index := Integer(Model.Vertex_Indices.Last_Index);
            end if;
        end loop;

        Put_Line("Finished parsing Obj.");

        return Model;

    end Parse;

end Cargame.Obj_Parser;
