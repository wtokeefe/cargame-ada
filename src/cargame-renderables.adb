with Interfaces;

with Ada.Text_IO;              use Ada.Text_IO;
with Ada.Containers;           use Ada.Containers;
with Ada.Containers.Hashed_Maps;

with GL;

with Cargame.Renderables;      use Cargame.Renderables;
with Cargame.Obj_Parser;
with Cargame.Util;             use Cargame.Util;

package body Cargame.Renderables is

   function Is_Actually_Renderable(R : Renderable_VAO) return Boolean is
      use type Ada.Containers.Count_Type;
   begin
      --  Put_Line("Is_Actually_Renderable on "
      --              & "R( Vao => " & UInt'Image(R.Vao.Raw_ID)
      --              &  ", Num_Indices => " & Int'Image(R.Num_Indices)
      --              &  ", Materials.Length => " & Count_Type'Image(R.Materials.Length)
      --              & ")");
      return (R.Vao /= Null_Array_Object
               and then R.Num_Indices /= 0
               and then R.Materials.Length /= 0);
   end Is_Actually_Renderable;

   procedure Render(R : in Renderable_VAO) is
      -- FIXME(wtok, 2018-03-24): When I don't re-bind the buffers,
      --  I get API errors suggesting I'm rendering unbound
      --  buffers. But they should be attached to the VAO, so I
      --  should just need to bind the VAO. What's going on?
      --  OpenGLAda bug?
      --
      -- UPDATE(2018-08-13): Works without binding on Linux, but not on Windows.
      --  Weird.
   begin
      Bind(R.Vao);
      --  Bind(Array_Buffer, R.Vertex_Buf);
      --  Bind(Element_Array_Buffer, R.Index_Buf);
      Draw_Elements(Mode           => Triangles,
                    Index_Type     => UInt_Type,
                    Element_Offset => 0,
                    Count          => R.Num_Indices);
      Unbind_VAO;
   end Render;

   function Get_Bounding_Volume(Verts : in Vector3_Array) return Volume3D_Type is
      Min, Max : Vector3 := (others => 0.0);
   begin
      for Vec of Verts loop
         for I in Vec'Range loop
            if Vec(I) > Max(I) then Max(I) := Vec(I); end if;
            if Vec(I) < Min(I) then Min(I) := Vec(I); end if;
         end loop;
      end loop;

      return Volume3D_Type(Max - Min);
   end Get_Bounding_Volume;

   function Create_Renderable_VAO(Vertices, Normals : in Vector3_Array;
                                  TexCrds           : in Vector2_Array;
                                  Indices           : in UInt_Array;
                                  Materials         : in Vector_Of_Material)
       return Renderable_VAO
   is
      Vao        : Vertex_Array_Object;
      Vertex_Buf : Buffer;
      Normal_Buf : Buffer;
      TexCrd_Buf : Buffer;
      Index_Buf  : Buffer;
      Out_Materials : Vector_Of_Material := Materials;
   begin
      Vao.Initialize_Id;
      pragma Assert(Vao.Initialized, "Failed to initialise Vertex_Array_Object.");
      Vao.Bind;

      -------------------------
      -- Add vertices to VAO --
      -------------------------

      Vertex_Buf.Initialize_Id;
      pragma Assert(Vertex_Buf.Initialized, "Failed to initialise vertex buffer.");

      Bind(Array_Buffer, Vertex_Buf);
      Load_Vector3_Buffer(Target => Array_Buffer,
                          Data   => Vertices,
                          Usage  => Static_Draw);

      Set_Vertex_Attrib_Pointer(Index  => Vertices_Attribute,
                                Count  => 3, -- Floats per vertex.
                                Kind   => Single_Type,
                                Stride => 0,
                                Offset => 0);
      Enable_Vertex_Attrib_Array(Vertices_Attribute);

      if Normals'Length /= 0 then

         ------------------------
         -- Add normals to VAO --
         ------------------------

         Normal_Buf.Initialize_Id;
         pragma Assert(Normal_Buf.Initialized, "Failed to initialise normal buffer.");

         Bind(Array_Buffer, Normal_Buf);
         Load_Vector3_Buffer(Target => Array_Buffer,
                             Data   => Normals,
                             Usage  => Static_Draw);

         Set_Vertex_Attrib_Pointer(Index => Normals_Attribute,
                                   Count => 3, -- Floats per normal.
                                   Kind  => Single_Type,
                                   Stride => 0,
                                   Offset => 0);
         Enable_Vertex_Attrib_Array(Normals_Attribute);

      end if;

      if TexCrds'Length /= 0 then

         ------------------------
         -- Add texcrds to VAO --
         ------------------------

         TexCrd_Buf.Initialize_Id;
         pragma Assert(TexCrd_Buf.Initialized, "Failed to initialise texcrd buffer.");

         Bind(Array_Buffer, TexCrd_Buf);
         Load_Vector2_Buffer(Target => Array_Buffer,
                             Data   => TexCrds,
                             Usage  => Static_Draw);

         Set_Vertex_Attrib_Pointer(Index  => TexCrds_Attribute,
                                   Count  => 2, -- Floats per texcrd.
                                   Kind   => Single_Type,
                                   Stride => 0,
                                   Offset => 0);
         Enable_Vertex_Attrib_Array(TexCrds_Attribute);

      end if;

      ------------------------
      -- Add indices to VAO --
      ------------------------

      Index_Buf.Initialize_Id;
      pragma Assert(Index_Buf.Initialized, "Failed to initialise index buffer.");

       Bind(Element_Array_Buffer, Index_Buf);
       Load_UInt_Buffer(Target => Element_Array_Buffer,
                        Data   => Indices,
                        Usage  => Static_Draw);

		 ---------------------------------------
		 -- Add default material if necessary --
		 ---------------------------------------

       if Out_Materials.Length = 0 then
          Put_Line("Appending default materials.");
          pragma Assert(Default_Material.Diffuse_Texture.Raw_ID /= 0
             					and then Default_Material.Specular_Texture.Raw_ID /= 0,
                        "Default material is uninitialised.");

          declare
             Mtl : Material := Default_Material;
          begin
             pragma Assert((for all I in Indices'First .. Indices'Last => Indices(I) = UInt(I)),
                           "Using default material for non-sequential indices"
                           & " is untested.");
             Mtl.First_Index := Integer(Indices'First);
             Mtl.Final_Index := Integer(Indices'Last);
             Out_Materials.Append(Mtl);
          end;
       else
          Put_Line("NOT appending default material.");
   	 end if;

       ------------
       -- Return --
       ------------

       Unbind_VAO;

       return Renderable_VAO'(Vao         => Vao,
                              Materials   => Out_Materials,
                              Dimensions  => Get_Bounding_Volume(Vertices),
                              Vertex_Buf  => Vertex_Buf,
                              Normal_Buf  => Normal_Buf,
                              TexCrd_Buf  => TexCrd_Buf,
                              Index_Buf   => Index_Buf,
                              Num_Indices => Indices'Length);

   end Create_Renderable_VAO;

   -- Create renderable from obj file. This means reduplicating unique
   --  vertex/normal/texcoord instances to be GL vertex attributes.
   function Create_Renderable_VAO(Obj_File_Path : in String) return Renderable_VAO is

       use Cargame.Obj_Parser;

       Model : constant Obj_Data := Parse(Obj_File_Path);

       package GL_Data is
           Vertices : Vector_Of_Vector3;
           Normals  : Vector_Of_Vector3;
           TexCrds  : Vector_Of_Vector2;
           Indices  : Vector_Of_UInt;
           -- Indices passed to glDrawElements and such. Starts at 0.
           Next_GL_Index : UInt := 0;
       end GL_Data;

       type GL_Vertex is record
           UV_Idx, UT_Idx, UN_Idx : UInt;
       end record;

       function "="(A, B : GL_Vertex) return Boolean is (A.UV_Idx = B.UV_Idx and then A.UN_Idx = B.UN_Idx);

       function Hash_GL_Vertex(X : GL_Vertex) return Hash_Type
         -- We need to encode 3*32-bit indices into what I assume is a 64-bit hash type, so
         --  each gets 21 bits to work with. If this precondition fails, we're guaranteed to
         --  have hash collisions.
         with Pre => (           X.UV_Idx < 2#0000_0000_0001_1111_1111_1111_1111_1111#
                        and then X.UN_Idx < 2#0000_0000_0001_1111_1111_1111_1111_1111#
                        and then X.UT_Idx < 2#0000_0000_0001_1111_1111_1111_1111_1111#)
       is
          use Interfaces;
          UV64 : constant Unsigned_64 := Unsigned_64(X.UV_Idx);
          UN64 : constant Unsigned_64 := Unsigned_64(X.UN_Idx);
          UT64 : constant Unsigned_64 := Unsigned_64(X.UT_Idx);
       begin
          return Hash_Type(    Shift_Left(UV64, 0 * (Hash_Type'Size / 3))
                             + Shift_Left(UN64, 1 * (Hash_Type'Size / 3))
                             + Shift_Left(UT64, 2 * (Hash_Type'Size / 3)));
       end Hash_GL_Vertex;

       package GLV_Map is new Ada.Containers.Hashed_Maps
         (Key_Type        => GL_Vertex,
          Element_Type    => UInt,
          Hash            => Hash_GL_Vertex,
          Equivalent_Keys => "=");

       Map   : GLV_Map.Map;
       Triad : GL_Vertex;

       use GLV_Map;

       Using_TexCrds : Boolean;
       Using_Normals : Boolean;

       begin

          Using_Normals := (Model.Normal_Indices.Length > 0);
          Using_TexCrds := (Model.TexCrd_Indices.Length > 0);

          ---------------------------------------
          -- Deduplicate obj data into GL data --
          ---------------------------------------

          for I in Model.Vertex_Indices.First_Index .. Model.Vertex_Indices.Last_Index loop
             Triad := GL_Vertex'(UV_Idx => Model.Vertex_Indices(I),
                                 UT_Idx => (if Using_TexCrds then Model.TexCrd_Indices(I) else 0),
                                 UN_Idx => (if Using_Normals then Model.Normal_Indices(I) else 0));

             if Map.Find(Triad) /= No_Element then
                -- We've already encountered this VTN triad before, so it must have
                --  an index. Append the index and don't create new vertex data.
                GL_Data.Indices.Append(Map.Element(Triad));
             else
                -- We've never seen this particular VTN triad before, so create new
                --  vertex data and assign the next index.
                GL_Data.Vertices.Append(Model.Unique_Vertices(Positive(Model.Vertex_Indices.Element(I))));

                if Model.Normal_Indices.Length /= 0 then
                   GL_Data.Normals.Append(Model.Unique_Normals(Positive(Model.Normal_Indices.Element(I))));
                end if;

                if Model.TexCrd_Indices.Length /= 0 then
                   GL_Data.TexCrds.Append(Model.Unique_TexCrds(Positive(Model.TexCrd_Indices.Element(I))));
                end if;

                GL_Data.Indices.Append(GL_Data.Next_GL_Index);

                -- NOTE(wtok, 2018-06-06): Comment below was written by me sometime
                --  in the past. It implies that First and Final vertex indices
                --  should be set in the material *before* we get here. It describes
                --  how we should modify the index ranges in the materials. We'll be
                --  reduplicating some VTN triads in this function, so the indices
                --  will need to be updated.
                --
                -- Old comment:
                --
                -- Each Material has a First and Final vertex index, which is the
                --  index to the Unique_Vertices array (which we call Model.Unique_Vertices in here).
                --  Basically, if our index to UV, which is Int(Model.Vertex_Indices.Element(I)), is
                --  equal to the First or Final_Vertex_Index for a Material, we need
                --  to update that value to be an index to Indices here -- that is,
                --  it should NOT be the value of Next_GL_Index.
                --
                -- FIXME: Does that make sense?
                --
                -- Later, when we call Render, we need to iterate over material
                --  ranges and use separate drawcalls per material.

                declare
                   type Bool_Array is array (Int range <>) of Boolean;
                   Changed_First : Bool_Array(1 .. Int(Model.Materials.Length)) := (others => False);
                   Changed_Final : Bool_Array(1 .. Int(Model.Materials.Length)) := (others => False);
                begin
                   for I in Model.Materials.Iterate loop
                      declare
                         Idx : constant Int := Int(Material_Vectors.To_Index(I));
                         Mtl : Material := Model.Materials(I);
                      begin
                         if not Changed_First(Idx)
                           and then Mtl.First_Index = Integer(GL_Data.Next_GL_Index)
                         then
                            Changed_First(Idx) := True;
                            Mtl.First_Index := Integer(GL_Data.Indices.Length);
                         elsif not Changed_Final(Idx)
                           and then Mtl.Final_Index = Integer(GL_Data.Next_GL_Index)
                         then
                            Changed_Final(Idx) := True;
                            Mtl.Final_Index := Integer(GL_Data.Indices.Length + 1);
                         end if;
                      end;
                   end loop;
                end;

                Map.Insert(Triad, GL_Data.Next_GL_Index);
                GL_Data.Next_GL_Index := GL_Data.Next_GL_Index + 1;
              end if;
           end loop;

           ---------------------------
           -- Construct output data --
           ---------------------------

           declare
               Out_Vertices : Vector3_Array(Int(1) .. Int(GL_Data.Vertices.Length));
               Out_Normals  : Vector3_Array(Int(1) .. Int(GL_Data.Normals.Length));
               Out_Indices  :    UInt_Array(Int(1) .. Int(GL_Data.Indices.Length));
               Out_TexCrds  : Vector2_Array(Int(1) .. Int(GL_Data.TexCrds.Length));
           begin
               for I in Out_Vertices'Range loop
                   Out_Vertices(I) := GL_Data.Vertices.Element(Integer(I));
               end loop;

               for I in Out_Normals'Range loop
                   Out_Normals(I) := GL_Data.Normals.Element(Integer(I));
               end loop;

               for I in Out_TexCrds'Range loop
                   Out_TexCrds(I) := GL_Data.TexCrds.Element(Integer(I));
               end loop;

               for I in Out_Indices'Range loop
                   Out_Indices(I) := GL_Data.Indices.Element(Integer(I));
               end loop;

               -- It's tempting to inline this function here, but it
               --  has some valuable preconditions for checking the
               --  data that I'd rather not duplicate.
               return Create_Renderable_VAO(Vertices  => Out_Vertices,
                                            Normals   => Out_Normals,
                                            TexCrds   => Out_TexCrds,
                                            Indices   => Out_Indices,
                                            Materials => Model.Materials);
           end;

   end Create_Renderable_VAO;

end Cargame.Renderables;
