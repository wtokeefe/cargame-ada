with Ada.Containers.Vectors;
with Ada.Directories;
with GL.Attributes;
with GL.Types;
with GL.Objects.Vertex_Arrays;
with GL.Objects.Buffers;

with Cargame.Types;
with Cargame.Vectors;
with Cargame.Obj_Parser;

package Cargame.Renderables is

   use Ada.Directories;
   use GL.Attributes;
   use GL.Types;
   use GL.Types.Singles;
   use GL.Objects.Vertex_Arrays;
   use GL.Objects.Buffers;
   use Cargame.Types;
   use Cargame.Vectors;

   Vertices_Attribute : constant Attribute := 0;
   Normals_Attribute  : constant Attribute := 1;
   TexCrds_Attribute  : constant Attribute := 2;

   -- Renderable_VAO implements the Renderable interface for OpenGL vertex array objects, and
   --  provides some handy creation functions.

   type Renderable_VAO is record
       Materials   : Vector_Of_Material;
       Vao         : Vertex_Array_Object := Null_Array_Object;
       Dimensions  : Volume3D_Type := (others => 0.0);
       Vertex_Buf  : Buffer;
       Normal_Buf  : Buffer;
       TexCrd_Buf  : Buffer;
       Index_Buf   : Buffer;
       Num_Indices : Int := 0;
   end record;

   function "="(L, R : in Renderable_VAO) return Boolean is
       (            (L.Vao         = R.Vao)
       --  and then (L.Materials = R.Materials)
           and then (L.Vertex_Buf  = R.Vertex_Buf)
           and then (L.Normal_Buf  = R.Normal_Buf)
           and then (L.TexCrd_Buf  = R.TexCrd_Buf)
           and then (L.Index_Buf   = R.Index_Buf)
           and then (L.Num_Indices = R.Num_Indices));

   package Prefab_RVAOs is
      Barrel : Renderable_VAO;
      Car    : Renderable_VAO;
   end Prefab_RVAOs;

   subtype RVAO_Vector_Index_Type is Int range 1 .. (Int'Last - 1);
   package Renderable_VAO_Vectors is new Ada.Containers.Vectors
       (RVAO_Vector_Index_Type, Cargame.Renderables.Renderable_VAO);
   subtype Vector_Of_Renderable_VAO is Renderable_VAO_Vectors.Vector;

   procedure Render(R : in Renderable_VAO)
       with Pre => Is_Actually_Renderable(R);

   function Is_Actually_Renderable(R : Renderable_VAO) return Boolean;

   function Create_Renderable_VAO(Vertices, Normals : in Vector3_Array;
                                  TexCrds           : in Vector2_Array;
                                  Indices           : in UInt_Array;
                                  Materials         : in Vector_Of_Material)
       return Renderable_VAO
       -- TODO(wtok, 2018-03-30): Add sanity checks for Normals and TexCrds.
       with Pre => (             (Vertices'Length /= 0)
                        and then (Indices'Length  /= 0)
                        and then (Indices'Length rem 3 = 0)
                        and then (for all V of Vertices => (for all X of V => X'Valid))
                        and then (for all V of Normals  => (for all X of V => X'Valid))
                        and then (for all V of TexCrds  => (for all X of V => X'Valid))),
            Post => Is_Actually_Renderable(Create_Renderable_VAO'Result);

   function Create_Renderable_VAO(Obj_File_Path : in String) return Renderable_VAO
     with Pre  => Exists(Obj_File_Path),
          Post => Is_Actually_Renderable(Create_Renderable_VAO'Result);

end Cargame.Renderables;
