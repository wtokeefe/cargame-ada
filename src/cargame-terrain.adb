with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Containers;      use Ada.Containers;

with GL;                  use GL;
with GL.Types;            use GL.Types; use GL.Types.Singles;

with Cargame.Renderables; use Cargame.Renderables;
with Cargame.Vectors;     use Cargame.Vectors;
with Cargame.Types;       use Cargame.Types;
with Cargame.Util;

package body Cargame.Terrain is
	function Generate_Terrain_Segment return Entity.Entity
	is
		N : constant Positive := 32;

		Vertices : Vector_Of_Vector3;
		Normals  : Vector_Of_Vector3;

		-- FIXME: What is this? I believe it's the number of vertices there are on
		--  the horizontal and depth axis of the terrain. In the C++ implementation
		--  it was parameterised, but I passed 64.
	begin
		for I in 1 .. N loop
			for J in 1 .. N loop
				declare
					FI : constant Single := Single(I);
					FJ : constant Single := Single(J);
					FN : constant Single := Single(N);
				begin
					Vertices.Append((X => (FI + 0.0) / FN, Y => 0.0, Z => (FJ + 0.0) / FN));
					Vertices.Append((X => (FI + 0.0) / FN, Y => 0.0, Z => (FJ + 1.0) / FN));
					Vertices.Append((X => (FI + 1.0) / FN, Y => 0.0, Z => (FJ + 1.0) / FN));
					Vertices.Append((X => (FI + 1.0) / FN, Y => 0.0, Z => (FJ + 1.0) / FN));
					Vertices.Append((X => (FI + 1.0) / FN, Y => 0.0, Z => (FJ + 1.0) / FN));
					Vertices.Append((X => (FI + 0.0) / FN, Y => 0.0, Z => (FJ + 0.0) / FN));
				end;
			end loop;
		end loop;

		pragma Assert(Vertices.Length /= 0,
						  "Appended a bunch of terrain vertices, but Vertices is empty.");

		for I in Vertices.First_Index .. Vertices.Last_Index loop
			Normals.Append((X => 0.0, Y => 1.0, Z => 0.0));
		end loop;

		pragma Assert(Normals.Length = Vertices.Length,
						  "Must have as many normals as vertices."
						   & " Have " & Count_Type'Image(Normals.Length) & " normals, and "
						   & Count_Type'Image(Vertices.Length) & " vertices." );

		pragma Assert((for all N of Normals => N = (0.0, 1.0, 0.0)),
		              "Not all Normals are (0, 1, 0)");

		declare
			Out_Indices   : UInt_Array    (Int(1) .. Int(Vertices.Length));
			Out_Vertices  : Vector3_Array (Int(1) .. Int(Vertices.Length));
			Out_Normals   : Vector3_Array (Int(1) .. Int(Vertices.Length));
			Out_TexCrds   : Vector2_Array (Int(1) .. Int(Vertices.Length));
			Out_Materials : Vector_Of_Material; -- Empty
		begin

			Util.Got_Here("Setting Out_Indices(" & Integer'Image(Out_Indices'Length) & " )");
			for I in Out_Indices'Range loop
				Out_Indices(I) := UInt(Positive(I));
			end loop;

			Util.Got_Here("Setting Out_Vertices(" & Integer'Image(Out_Vertices'Length) & " )");
			for I in Out_Vertices'Range loop
				Out_Vertices(I) := Vertices(Positive(I));
			end loop;

			Util.Got_Here("Setting Out_Normals(" & Integer'Image(Out_Normals'Length) & " )");
			for I in Out_Normals'Range loop
				Out_Normals(I) := (0.0, 1.0, 0.0);
			end loop;

			Util.Got_Here("Setting Out_TexCrds(" & Integer'Image(Out_TexCrds'Length) & " )");
			for I in Out_TexCrds'Range loop
				Out_TexCrds(I) := (0.0, 0.0);
			end loop;

   		return Entity.Entity'(RVAO => Create_Renderable_VAO
   		                     	(Vertices  => Out_Vertices,
											 Normals   => Out_Normals,
											 TexCrds   => Out_TexCrds,
											 Indices   => Out_Indices,
											 Materials => Out_Materials),
										 Is_Live => True,
   									 others  => <>);
		end;
	end Generate_Terrain_Segment;
end Cargame.Terrain;
