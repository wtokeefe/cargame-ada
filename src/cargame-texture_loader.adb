with System;
with Ada.Text_IO; use Ada.Text_IO;
with GL.Pixels;
with GL.Objects.Textures.Targets;

with Ada.Unchecked_Conversion;

--
-- This is just a non-comprehensive wrapper around stb_image.h.
--

package body Cargame.Texture_Loader is
   use Interfaces.C.Strings;
   use GL.Types;
   use GL.Objects.Textures;
   use GL.Objects.Textures.Targets;
   use GL.Objects.Textures.Targets.Texture_2D_Target;

   -- stbi_uc *stbi_load (char const *filename,
   --                     int *x, int *y, int *channels_in_file,
   --                     int desired_channels);
   function STBI_Load(Filename               : in     Chars_Ptr;
                      X, Y, Channels_In_File :    out GL.Types.Int;
                      Desired_Channels       : in     GL.Types.Int)
      return Chars_Ptr
         with Import, Convention => C, External_Name => "stbi_load",
              Post => Interfaces.C.Is_Nul_Terminated(Value(STBI_Load'Result));

   procedure STBI_Image_Free(Data : in Chars_Ptr)
      with Import, Convention => C, External_Name => "stbi_image_free";

   function STBI_Failure_Reason return Interfaces.C.Strings.Chars_Ptr
      with Import, Convention => C, External_Name => "stbi_failure_reason";

   procedure Load_Texture(Filename : in     String;
                          Tex      : in out Texture)
   is
      Image_Data : Chars_Ptr;
      Width, Height, Channels_In_File : Int := -1;
      Desired_Channels : constant Int := 3;

      Filename_C : Chars_Ptr := New_String(Filename);

      function To_Image_Source is
         new Ada.Unchecked_Conversion(Chars_Ptr, Image_Source);

   begin
      Texture_2D.Bind(Tex);

      Image_Data := STBI_Load(Filename         => Filename_C,
               X                => Width,
               Y                => Height,
               Channels_In_File => Channels_In_File,
               Desired_Channels => Desired_Channels);

      Free(Filename_C);

      --  Put_Line("Loaded image with (Width => " & Int'Image(Width)
      --               & ", Height => " & Int'Image(Height)
      --               & ", Channels => " & Int'Image(Channels_In_File)
      --               & ")");

      pragma Assert(Width > 0 and Height > 0 and Channels_In_File > 0,
               "STBI_Load error: " & (if STBI_Failure_Reason /= Null_Ptr
                      then Value(STBI_Failure_Reason)
                      else "UNKNOWN"));

      Texture_2D.Load_From_Data(Level           => Mipmap_Level(0),
                 Internal_Format => GL.Pixels.RGB,
                 Width           => Width,
                 Height          => Height,
                 Source_Format   => GL.Pixels.RGB,
                 Source_Type     => GL.Pixels.Unsigned_Byte,
                 Source          => To_Image_Source(Image_Data));

      Generate_Mipmap(Texture_2D);

      STBI_Image_Free(Image_Data);

      --  Texture_2D.Bind(0);

   end Load_Texture;

   function Load_Texture(Filename : in String) return Texture is
      Ret : Texture;
   begin
      Load_Texture(Filename, Ret);
      return Ret;
   end Load_Texture;

end Cargame.Texture_Loader;
