with Ada.Directories;
with Interfaces.C;
with Interfaces.C.Strings;
with GL.Types;
with GL.Objects.Textures;

package Cargame.Texture_Loader is
	use GL.Types;
   use GL.Objects.Textures;

   function Load_Texture(Filename : in String) return Texture
      with Pre  => Ada.Directories.Exists(Filename),
           Post => Load_Texture'Result.Raw_ID /= 0;

end Cargame.Texture_Loader;
