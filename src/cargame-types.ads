with Ada.Numerics;
with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Strings.Bounded;
with Ada.Strings.Unbounded;
with Ada.Real_Time;

with GL;
with GL.Objects.Buffers;
with GL.Objects.Textures;
with GL.Types;

package Cargame.Types is

   use GL;
   use GL.Types;
   use GL.Types.Singles;
   use GL.Objects.Buffers;

   function "="(L, R : in GL.Objects.Textures.Texture) return Boolean is
   	(L.Raw_ID = R.Raw_ID);

   package Single_Elementary_Functions is
       new Ada.Numerics.Generic_Elementary_Functions(Single);
   use Single_Elementary_Functions;

   function Image(V : in Vector2) return String is
       ("( " & Single'Image(V(X)) & ", "
       		 & Single'Image(V(Y)) & " )");

   function Image(V : in Vector3) return String is
       ("( " & Single'Image(V(X)) & ", "
             & Single'Image(V(Y)) & ", "
             & Single'Image(V(Z)) & " )");

   function Is_Valid(V : Vector3) return Boolean is (for all X of V => X'Valid) with Inline;
   function Is_Valid(V : Vector2) return Boolean is (for all X of V => X'Valid) with Inline;

   -----------
   -- Units --
   -----------

   type Radians is new Single;
   type Degrees is new Single;

   Radians_Per_Degree : constant Radians := Ada.Numerics.Pi / 180.0;
   Degrees_Per_Radian : constant Degrees := 180.0 / Ada.Numerics.Pi;

   function To_Radians(X : Degrees) return Radians is (Radians(X) * Radians_Per_Degree);
   function To_Degrees(X : Radians) return Degrees is (Degrees(X) * Degrees_Per_Radian);

   subtype Valid_Vector3 is Vector3
   	with Dynamic_Predicate => Is_Valid(Valid_Vector3);

   type Distance_Type is new Valid_Vector3;
   type Position_Type is new Valid_Vector3;
   type Velocity_Type is new Valid_Vector3; -- Units per second. NOT units per frame.

   type Volume3D_Type is new Valid_Vector3
     with Dynamic_Predicate => (for all Val of Volume3D_Type => Val >= 0.0);

   function "+"(L, R : Position_Type) return Position_Type is abstract;
   function "-"(L, R : Position_Type) return Distance_Type is (Distance_Type(Vector3(L) - Vector3(R)));

   function "+"(L : Position_Type; R : Distance_Type) return Position_Type is (Position_Type(Vector3(L) + Vector3(R)));
   function "-"(L : Position_Type; R : Distance_Type) return Position_Type is (L + (-R));

   -- Common operation. Doesn't make sense units-wise, but should be readable enough.
   function "+"(L : Position_Type; R : Velocity_Type) return Position_Type is (Position_Type(Vector3(L) + Vector3(R)));
   function "-"(L : Position_Type; R : Velocity_Type) return Position_Type is (L + (-R));

   use Ada.Real_Time;
   function "/"(L : Distance_Type; R : Single)    return Distance_Type is (X => L(X)/R, Y => L(Y)/R, Z => L(Z)/R);

	-- NOTE: Velocity_Type is in metres per second. TODO: Codify that.
   function "/"(L : Distance_Type; R : Time_Span) return Velocity_Type is (Velocity_Type(L / Single(R / Seconds(1))));

   --------------------
   -- Buffer loaders --
   --------------------

   procedure Load_UInt_Buffer    is new Load_To_Buffer(GL.Types.UInt_Pointers);
   procedure Load_Vector2_Buffer is new Load_To_Buffer(Vector2_Pointers);
   procedure Load_Vector3_Buffer is new Load_To_Buffer(Vector3_Pointers);
   --  procedure Load_Vector4_Buffer is new Load_To_Buffer(Vector4_Pointers);

   -------------------------
   -- Geometric functions --
   -------------------------


   procedure Look_At(Camera_Position, Target_Position, Up : Vector3;
                     Mtx : out Matrix4);
   function Look_At(Camera_Position, Target_Position, Up : in Vector3)
                   return Matrix4;

   procedure Init_Orthographic_Transform
       (Top, Bottom, Left, Right, Z_Near, Z_Far : Single;
        Transform                               : out Matrix4);

   function Perspective_Matrix(Top, Bottom, Left, Right, Near, Far : Single)
       return Matrix4;
   function Perspective_Matrix(View_Angle : Degrees;
                               Aspect_Ratio, Near, Far : Single)
       return Matrix4;
   function Rotation_Matrix(Angle : Radians; Axis : Vector3) return Matrix4;
   procedure Rotate(Mtx   : in out Matrix4;
                    Angle : in Radians;
                    Axis  : in Vector3 := (Y => 1.0, others => 0.0));

   procedure Translate(Mtx : in out Matrix4; Change : Vector3) with Inline;
   function  Translate(Mtx : in     Matrix4; Change : Vector3) return Matrix4 with Inline;

   -- TODO(wtok, 2018-07-04): write Transpose.
   --  function Transpose(M : in Matrix4) return Matrix4;
   function Inverse(M : in Matrix4) return Matrix4;
   function Inverse(M : in Matrix3) return Matrix3;

   function  Scale(Mtx : in     Matrix4; Factor : in Single) return Matrix4 with Inline;
   procedure Scale(Mtx : in out Matrix4; Factor : in Single) with Inline;

   ------------------------------
   -- Vector utility functions --
   ------------------------------

   function Normalized(V : Vector3) return Vector3;

   function Square    (N : Single) return Single is (N*N) with Inline;
   function Sum       (V : Vector3) return Single is (V(X)+V(Y)+V(Z)) with Inline;
   function Map       (V : in Vector3;
                       F : not null access function(X : Single) return Single)
       return Vector3 is (Vector3'(F(V(X)), F(V(Y)), F(V(Z)))) with Inline;

   function Length(V : Vector3) return Single is -- Actual length, in 3D space. Not array length.
       (Sqrt(Sum(Map(V, Square'Access)))) with Inline;

   function Mat4_To_Mat3(M : in Matrix4) return Matrix3 is
       (Matrix3'(X => (M(X, X), M(X, Y), M(X, Z)),
                 Y => (M(Y, X), M(Y, Y), M(Y, Z)),
                 Z => (M(Z, X), M(Z, Y), M(Z, Z))));

   --------------
   -- Mtl data --
   --------------

   use Ada.Strings.Unbounded;
   use GL.Objects.Textures;

   Max_Material_Name_Length : constant := 100;
   package Material_Names is
      new Ada.Strings.Bounded.Generic_Bounded_Length(Max_Material_Name_Length);
   subtype Material_Name is Material_Names.Bounded_String;

   function To_Material_Name(S : in Unbounded_String) return Material_Name is 
     (Material_Names.To_Bounded_String(Ada.Strings.Unbounded.To_String(S)))
      with Pre => Length(S) < Max_Material_Name_Length;

   function To_Material_Name(S : in String) return Material_Name is
      (Material_Names.To_Bounded_String(S))
        with Pre => S'Length < Max_Material_Name_Length;

   type Material is record
      Name               : Material_Name;
      Ambient_Light      : Vector3 := (others => 0.0);
      Diffuse_Light      : Vector3 := (others => 0.0);
      Specular_Light     : Vector3 := (others => 0.0);
      First_Index        : Integer := -1;
      Final_Index        : Integer := -1;
      Diffuse_Texture    : Texture;
      Specular_Texture   : Texture;
      Shininess          : Single := 8.0;
   end record;

   function Get_Material_Name(M : in Material) return String is
      ("Material[ " & Material_Names.To_String(M.Name) & " ]");

   function Num_Indices(M : in Material) return Int is
      (Int(M.Final_Index - M.First_Index + 1));

   use Material_Names;
   function "="(L, R : in Material) return Boolean is
      (L.Name = R.Name
           and then L.Ambient_Light           = R.Ambient_Light
           and then L.Diffuse_Light           = R.Diffuse_Light
           and then L.Specular_Light          = R.Specular_Light
           and then L.First_Index             = R.First_Index
           and then L.Final_Index             = R.Final_Index
           and then L.Diffuse_Texture.Raw_Id  = R.Diffuse_Texture.Raw_Id
           and then L.Specular_Texture.Raw_Id = R.Diffuse_Texture.Raw_Id
           and then L.Shininess               = R.Shininess);

	Default_Material : Material;

end Cargame.Types;
