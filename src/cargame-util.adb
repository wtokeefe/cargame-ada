with Ada.Text_IO;
with Ada.Strings.Unbounded.Text_IO;
with Ada.Strings;
with Ada.Strings.Fixed;

with GL.Objects.Vertex_Arrays;
with GL.Types;

with Glfw.Windows;
with Glfw.Input.Mouse;

with Cargame.Types;
with Cargame.Uniforms;

package body Cargame.Util is

   -- Print an informative message
   procedure Got_Here(Source_Location : in String := GNAT.Source_Info.Source_Location;
                      Context         : in String := GNAT.Source_Info.Enclosing_Entity) is
   begin
      Ada.Text_IO.Put_Line("Got here: " & Context & " @ " & Source_Location);
   end Got_Here;

    package UBIO renames Ada.Strings.Unbounded.Text_IO;

    function Split_Into_Tokens(Str : in Unbounded_String;
                               Set : in Character_Set)
        return UB_String_Vector
    is
        First      : Positive := 1;
        Last       : Natural  := Length(Str);
        Out_Vector : UB_String_Vector;
    begin
        loop
            Ada.Strings.Fixed.Find_Token
                (Source => Slice(Str, First, Last),
                 Set    => Set,
                 Test   => Ada.Strings.Inside,
                 First  => First,
                 Last   => Last);

            exit when Last = 0;

            Out_Vector.Append( Unbounded_Slice(Str, First, Last) );
            First := Last + 1;
            Last := Length(Str);
        end loop;

        return Out_Vector;

    end Split_Into_Tokens;

    procedure Unbind_VAO is
        use GL.Objects.Vertex_Arrays;
    begin
        Bind(Null_Array_Object);
    end Unbind_VAO;

    function UB_String_Vector_Image(V : in UB_String_Vector) return String is
        -- SPEEDUP(wtok, 2018-04-06): Really naive, probably causes multiple
        --  allocations as UB_Ret grows. Pre-allocate the string and fill it.
        UB_Ret : Unbounded_String;
    begin
        for UB_Str of V loop
            UB_Ret := UB_Ret & "[" & UB_Str & "]";
        end loop;
        return To_String(UB_Ret);
    end UB_String_Vector_Image;

    use Cargame.Types;

    --  function Raycast_Cursor return Position_Type is
        --  use Glfw.Windows;
        --  use GL.Types.Singles;
        --  Inv_Camera         : constant Matrix4 := Inverse(Uniforms.Camera_Transform.Get);
        --  Cursor_X, Cursor_Y : Glfw.Input.Mouse.Coordinate;
        --  Mapped             : Vector4;
        --  Result             : Vector4;
    --  begin
        --  Globals.Main_Window.Get_Cursor_Pos(Cursor_X, Cursor_Y);
        --  Mapped := Vector4'(X => -1.0 + (2.0 * Single(Cursor_X) / Single(Globals.Main_Window_Width)),
                           --  Y => +1.0 - (2.0 * Single(Cursor_Y) / Single(Globals.Main_Window_Height)),
                           --  Z => -1.0,
                           --  W => 1.0);
        --  Result := Inv_Camera * Mapped;
        --  return Position_Type(Normalized(Vector3'(X => Result(X), Y => Result(Y), Z => Result(Z))));
    --  end Raycast_Cursor;

end Cargame.Util;
