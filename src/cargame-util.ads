with GNAT.Source_Info;

with Ada.Real_Time; use Ada.Real_Time;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;
with Ada.Strings.Maps;
with Cargame.Types;

with GL;          use GL;
with GL.Types;    use GL.Types;
with GL.Uniforms; use GL.Uniforms;

package Cargame.Util is

        use Ada.Containers;
        use Ada.Strings.Unbounded;
        use Ada.Strings.Maps;

        package UB_String_Vectors is new Ada.Containers.Vectors
            (Index_Type   => Positive,
             Element_Type => Unbounded_String);
        subtype UB_String_Vector is UB_String_Vectors.Vector;

        -- Split a string into tokens that match some set. This is nice.

        function Split_Into_Tokens(Str : in Unbounded_String; Set : in Character_Set)
                return UB_String_Vector
            with Pre => (Length(Str) /= 0);

        function Split_Into_Tokens(Str : in String; Set : in Character_Set)
                return UB_String_Vector is (Split_Into_Tokens(To_Unbounded_String(Str), Set));

        function Time_Span_Image(TS : in Time_Span) return String is
                (Duration'Image(To_Duration(TS)) & " seconds?");

        Bad_Index : exception;

        function Int_To_Index(I : Integer) return Index_Homogeneous is
                (case I is
                    when 1 => X,
                    when 2 => Y,
                    when 3 => Z,
                    when 4 => W,
                    when others => raise Bad_Index)
                with Pre => (I >= 1 and then I <= 4);

        procedure Unbind_VAO;

        function UB_String_Vector_Image(V : in UB_String_Vector) return String;

        use Cargame.Types;

        --  function Raycast_Cursor return Position_Type;

        procedure Got_Here(Source_Location : in String := GNAT.Source_Info.Source_Location;
                           Context         : in String := GNAT.Source_Info.Enclosing_Entity);

end Cargame.Util;
